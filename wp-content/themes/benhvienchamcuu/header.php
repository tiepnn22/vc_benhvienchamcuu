<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <title><?php echo title(); ?></title>
	<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>

<?php
	use NF\View\Facades\View;
	echo View::render('partials.header');
?>
