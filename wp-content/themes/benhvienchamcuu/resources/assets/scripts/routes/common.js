import Wow from 'wow.js';

export default {
  init() {
    // JavaScript to be fired on all pages // viet vao day

    // menu sidebar
    $('.menu-menu-gioi-thieu-sidebar-container > ul > li').click(function(){
    	$(this).find('.sub-menu').stop(true,false).slideToggle();
    });

    $('.menu-menu-gioi-thieu-sidebar-container > ul > li').find('.sub-menu').parent().addClass('tamgiac');

    //menu primary
    if ($('.main-menu').length) {
        $('.main-menu').meanmenu({
            meanScreenWidth: "992",
            meanMenuContainer: ".mobile-menu",
        });
    }

    //back to top
    $(window).on('scroll', function () {
        if ($(window).scrollTop() > 20) {
            $("#back-to-top").css("display", "block");
        } else {
            $("#back-to-top").css("display", "none");
        }
    });
    if($('#back-to-top').length){
        $("#back-to-top").on('click', function() {
           $('html, body').animate({
               scrollTop: $('html, body').offset().top
             }, 1000);
        });
    }

    //slide doctor home
    if ($('.doctor-content .item').length > 1) {
        $('.doctor-content .row').slick({
            infinite: true,
            speed: 1000,
            slidesToShow: 4,
            slidesToScroll: 2,
            autoplay: true,
            autoplaySpeed: 3000,
            pauseOnHover: false,
            dots: false,
            arrows: true,
            prevArrow: '<span class="slick-prev"></span>',
            nextArrow: '<span class="slick-next"></span>',
            responsive: [{
                breakpoint: 1200,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 1
                }
            }, {
                breakpoint: 992,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1
                }
            }, {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }]
        });
    }

    //slide cac chuyen khoa home
    if ($('.service .item').length > 1) {
        $('.service .service-content').slick({
            infinite: true,
            speed: 1000,
            slidesToShow: 8,
            slidesToScroll: 2,
            autoplay: true,
            autoplaySpeed: 3000,
            pauseOnHover: false,
            dots: false,
            arrows: true,
            prevArrow: '<span class="slick-prev"></span>',
            nextArrow: '<span class="slick-next"></span>',
            responsive: [{
                breakpoint: 1200,
                settings: {
                    slidesToShow: 6,
                    slidesToScroll: 2
                }
            }, {
                breakpoint: 992,
                settings: {
                    slidesToShow: 4,
                    slidesToScroll: 2
                }
            }, {
                breakpoint: 767,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 1
                }
            }, {
                breakpoint: 480,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1
                }
            }]
        });
    }

    // const wow = new Wow();
    // wow.init();
    // new Wow().init();

    //facebook chat
    // $('.btn-fb-dialog').css({'display' : 'block', 'z-index' : '1000'});
    // $('.fb-iframe').css({'display' : 'none'});
    // var height_ifr = $('.fb-iframe').height();
    // console.log(height_ifr);
    // $('.btn-fb-dialog').click(function(){
    //     var show = $('.fb-iframe').attr('data-show');
    //     if(show == 'none') {
    //         $('.fb-iframe').css({'display' : 'block','position': 'fixed', 'z-index' : '1000', 'bottom' : '0', 'right' : '0'});
    //         $('.btn-fb-dialog').css({'position' : 'absolute', 'bottom' : height_ifr});
    //         $('.fb-iframe').attr('data-show', 'show');
    //         $('.btn-fb-dialog').text('Đóng x');
    //     } else {
    //         $('.fb-iframe').attr('data-show', 'none');
    //         $('.fb-iframe').css({'display' : 'none', 'bottom' : '0'});
    //         $('.btn-fb-dialog').css({'bottom' : '0'});
    //         $('.btn-fb-dialog').text('Gửi tin nhắn cho chúng tôi ');
    //     }
    // });


    // $('.header .header-bottom .menu .main-menu > ul > li > .sub-menu > li').hover(function(){
    //     var tiep = $(this).find('.sub-menu li');
    //     var tiep2 = tiep.length;
    //     if(tiep2 > 9){

    //     }
    // });


    var scroll = $('div').width();
    if(scroll >= 992) {
        var height = $('.header .header-bottom .menu .main-menu > ul > li .sub-menu li .sub-menu');
        var height2 = height.height();
        var $content = $('.header .header-bottom .menu .main-menu > ul > li .sub-menu');
        $content.height(height2);
    }


    $('.header .header-bottom .menu .main-menu > ul > li').hover(function(){
        $(this).find('.sub-menu > li:first-child > .sub-menu').css('visibility', 'visible');
    },function(e) {
        $(this).find('.sub-menu > li:first-child > .sub-menu').css('visibility', 'hidden');
    });


    },
    finalize() {
        // JavaScript to be fired on all pages, after page specific JS is fired
    },
};
