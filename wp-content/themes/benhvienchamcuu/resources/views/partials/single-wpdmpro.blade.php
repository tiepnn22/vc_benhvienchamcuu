<?php
	use NF\View\Facades\View;
	echo View::render('partials.home-slide');
?>


<section class="download-detail">
	<div class="container">
		<div class="row">
			<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12 download-detail-content">

				<?php if(have_posts()) : while (have_posts() ) : the_post(); ?>
					<div class="download-detail-info">
						<div class="single-title">
							<h1><?php the_title();?></h1>
						</div>
						<div class="page-info">
							<?php the_content(); ?>
						</div>
					</div>
				<?php endwhile; wp_reset_query(); else: echo ''; endif; ?>

			</div>

			<?php
				echo View::render('partials.sidebar');
			?>
		</div>
	</div>
</section>