<?php
	$terms = get_terms('chuyen-khoa-category', array(
        'parent'=> 0,
        'hide_empty' => false
    ) );
    foreach($terms as $term){
        $doctor_name = $term->name;
        $doctor_id = $term->term_id;
    }
	// $term_img = types_render_termmeta( "image-cat", array('term_id' => $term->term_id , 'class' => 'img-responsive') );
?>
<!-- <section class="banner"> -->
	<?php
		// echo $term_img;
	?>
<!-- </section> -->

<?php
	use NF\View\Facades\View;
	echo View::render('partials.home-slide');
?>

<section class="news-detail">
	<div class="container">
		<div class="row">
			<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12 news-detail-content">
				<div class="main-title">
					<a href="<?php echo esc_url(get_term_link($doctor_id));?>">
						<h2><?php echo $doctor_name;?></h2>
					</a>
				</div>

				<?php if(have_posts()) : while (have_posts() ) : the_post(); ?>
					<div class="news-detail-info">
						<div class="single-title">
							<h1><?php the_title();?></h1>
						</div>
						<div class="page-info">
							<?php the_content(); ?>
						</div>
					</div>
				<?php endwhile; wp_reset_query(); else: echo ''; endif; ?>


				<?php
					echo View::render('partials.comment-like-share');
					echo View::render('partials.related-chuyen-khoa');
					echo View::render('partials.news-event');
				?>
			</div>


			<?php
				echo View::render('partials.sidebar');
			?>
		</div>
	</div>
</section>