<?php
	$a = wp_get_post_categories(get_the_ID());
	$id_category = $a[0];
	$name_category = get_cat_name( $id_category );
	// $term_img = types_render_termmeta( "image-cat", array('term_id' => $id_category , 'class' => 'img-responsive') );
?>
<!-- <section class="banner"> -->
	<?php
		// echo $term_img;
	?>
<!-- </section> -->

<?php
	use NF\View\Facades\View;
	echo View::render('partials.home-slide');
?>

<section class="news-detail">
	<div class="container">
		<div class="row">
			<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12 news-detail-content">
				<div class="main-title">
					<a href="<?php echo esc_url(get_term_link($id_category));?>">
						<h2><?php echo $name_category;?></h2>
					</a>
				</div>

				<?php if(have_posts()) : while (have_posts() ) : the_post(); ?>
					<div class="news-detail-info">
						<div class="single-title">
							<h1><?php the_title();?></h1>
						</div>
						<div class="page-info">
							<?php the_content(); ?>
						</div>
					</div>
				<?php endwhile; wp_reset_query(); else: echo ''; endif; ?>


				<?php
					echo View::render('partials.comment-like-share');
					echo View::render('partials.related-post');
					echo View::render('partials.news-event');
				?>
			</div>


			<?php
				echo View::render('partials.sidebar');
			?>
		</div>
	</div>
</section>