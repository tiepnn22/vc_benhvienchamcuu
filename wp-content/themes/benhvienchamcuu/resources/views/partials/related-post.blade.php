<aside class="news related-post">
	<div class="main-title"><a><h2>Tin liên quan</h2></a></div>
	<div class="news-content">
		<div class="row">
			<?php
			    global $post;
			    $orig_post = $post;
			    $categories = get_the_category($post->ID);
			    if ($categories) {
				    $category_ids = array();
				    foreach($categories as $individual_category) $category_ids[] = $individual_category->term_id;
				    $args=array(
					    'category__in' => $category_ids,
					    'post__not_in' => array($post->ID),
					    'posts_per_page'=> 4,
					    'ignore_sticky_posts'=>1
				    );
				    $my_query = new wp_query( $args );
				    if( $my_query->have_posts() ) {
					    while( $my_query->have_posts() ) {
						    $my_query->the_post();?>

							    <article class="col-lg-3 col-md-3 col-sm-6 col-xs-6 item">
									<figure>
										<a href="<?php the_permalink();?>">
											<img src="<?php echo asset('images/3x2.png'); ?>" alt="<?php the_title(); ?>" style="background-image: url(<?php echo tiep_get_thumbnail_url('news');?>);" />
										</a>
									</figure>
									<div class="info">
										<div class="title">
											<a href="<?php the_permalink();?>" rel="bookmark">
												<h3><?php the_title();?></h3>
											</a>
										</div>
										<div class="desc">
											<?php echo cut_string(get_the_excerpt(),120,'...');?>
										</div>
										<a href="<?php the_permalink();?>" rel="bookmark" class="btn-more">Xem thêm >></a>
									</div>
								</article>

						    <?php
					    }
				    }
			    }
			    $post = $orig_post;
			    wp_reset_query();
			?>
		</div>
	</div>
</aside>


