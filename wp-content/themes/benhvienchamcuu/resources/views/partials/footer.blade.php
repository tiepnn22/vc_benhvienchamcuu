<section class="map">
	<div class="container">
		<div class="map-content">
			<?php dynamic_sidebar( 'footer-map' ); ?>
		</div>
	</div>
</section>


<section class="partner">
	<div class="container">
		<?php dynamic_sidebar( 'home-partner' ); ?>
	</div>
</section>


<footer>
	<div class="footer-top">
		<div class="container">
			<div class="row">
				<div class="col-lg-2 col-md-2 col-sm-2 col-xs-12 item">
					<img src="<?php echo get_theme_mod( 'img-upload' );?>">
				</div>
				<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 item">
					<?php dynamic_sidebar( 'footer-2' ); ?>
				</div>
				<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 item">
					<?php dynamic_sidebar( 'footer-3' ); ?>
				</div>
			</div>
		</div>
	</div>
	<div class="footer-bottom">
		<?php echo get_theme_mod( 'text_option' ); ?>
	</div>
</footer>


<div id="back-to-top">
    <a href="javascript:void(0)"><i class="fa fa-chevron-up" aria-hidden="true"></i></a>
</div>

<?php
	use NF\View\Facades\View;
	echo View::render('partials.facebook-chat');
?>