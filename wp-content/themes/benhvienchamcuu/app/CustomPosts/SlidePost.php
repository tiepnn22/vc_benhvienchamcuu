<?php

namespace App\CustomPosts;

use NF\Abstracts\CustomPost;

class SlidePost extends CustomPost
{
    public $type = 'slide_post';

    public $single = 'Slide';

    public $plural = 'Slides';

    public $args = ['menu_icon' => 'dashicons-camera'];
}
