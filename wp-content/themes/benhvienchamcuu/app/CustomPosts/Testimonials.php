<?php

namespace App\CustomPosts;

use NF\Abstracts\CustomPost;

class Testimonials extends CustomPost
{
    public $type = 'testimonial';

    public $single = 'Testimonial';

    public $plural = 'Testimonials';

    public $args = ['menu_icon' => 'dashicons-format-status'];

}
