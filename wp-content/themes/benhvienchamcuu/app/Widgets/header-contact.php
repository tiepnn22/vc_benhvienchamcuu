<?php
class contact_header extends WP_Widget {
    function __construct() {
        parent::__construct(
            'Contact-header',
            'Header - Liên hệ',
            array( 'description'  =>  'Header - Liên hệ' )
        );
    }
    function form( $instance ) {
        $default = array(
            'title' => '',
            'phone' => '',
            'email' => '',
            'linkphone' => '',
            'linkemail' => '',
        );
        $instance = wp_parse_args( (array) $instance, $default );
        $title = esc_attr($instance['title']);
        $phone = esc_attr($instance['phone']);
        $email = esc_attr($instance['email']);
        $linkphone = esc_attr($instance['linkphone']);
        $linkemail = esc_attr($instance['linkemail']);
        echo '<p>Tiêu đề:<input type="text" class="widefat" name="'.$this->get_field_name('title').'" value="'.$title.'"/></p>';
        echo '<p>Phone:<input type="text" class="widefat" name="'.$this->get_field_name('phone').'" value="'.$phone.'" /></p>';
        echo '<p>Email:<input type="text" class="widefat" name="'.$this->get_field_name('email').'" value="'.$email.'" /></p>';
        echo '<p>Link Phone:<input type="text" class="widefat" name="'.$this->get_field_name('linkphone').'" value="'.$linkphone.'" /></p>';
        echo '<p>Link Email:<input type="text" class="widefat" name="'.$this->get_field_name('linkemail').'" value="'.$linkemail.'" /></p>';
    }
    function update( $new_instance, $old_instance ) {
        $instance = $old_instance;
        $instance['title'] = strip_tags($new_instance['title']);
        $instance['phone'] = ($new_instance['phone']);
        $instance['email'] = ($new_instance['email']);
        $instance['linkphone'] = ($new_instance['linkphone']);
        $instance['linkemail'] = ($new_instance['linkemail']);
        return $instance;
    }
    function widget( $args, $instance ) {
        extract($args);
        $title = apply_filters( 'widget_title', $instance['title'] );
        $phone = $instance['phone'];
        $email = $instance['email'];
        $linkphone = $instance['linkphone'];
        $linkemail = $instance['linkemail'];

        echo $before_widget;
            echo '<a href="tel:'.$linkphone.'"><i class="fa fa-phone" aria-hidden="true"></i>'.$phone.'</a>';
            echo '<a href="mailto:'.$linkemail.'"><i class="fa fa-envelope-o" aria-hidden="true"></i>'.$email.'</a>';
        echo $after_widget;
    }
}
function create_contact_header_widget() {
    register_widget('contact_header');
}
add_action( 'widgets_init', 'create_contact_header_widget' );