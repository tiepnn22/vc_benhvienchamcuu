<?php
class show_partner extends WP_Widget {
    function __construct() {
        parent::__construct(
            'show_partner',
            'Trang chủ - Hợp tác chuyên môn',
            array( 'description'  =>  'Trang chủ - Hợp tác chuyên môn' )
        );
    }
    function form( $instance ) {
        $default = array(
            'title' => 'Hợp tác chuyên môn',
            'id_danh_muc' => 12,
            'custom_post_number' => 7,
        );
        $instance = wp_parse_args( (array) $instance, $default );

        $title = esc_attr($instance['title']);
        $id_danh_muc = esc_attr($instance['id_danh_muc']);
        $custom_post_number = esc_attr($instance['custom_post_number']);

        echo '<p>Tiêu đề:<input type="text" class="widefat" name="'.$this->get_field_name('title').'" value="'.$title.'"/></p>';
        echo '<p>Chọn taxonomy :<select name="'.$this->get_field_name('id_danh_muc').'">';
            //
            $terms = get_terms('hop-tac-category', array(
                'parent'=> 0,
                'hide_empty' => false
            ) );
            foreach($terms as $term){
                $team_lg = $term->term_id;
                if($id_danh_muc == $term->term_id) {
                    echo '<option name="'.$this->get_field_name('$term->term_id').'" selected value="'.$term->term_id.'">'.$term->name.'</option>';
                }else{
                    echo '<option name="'.$this->get_field_name('$term->term_id').'" value="'.$term->term_id.'">'.$term->name.'</option>';
                }
                //
                $term_childs = get_term_children( $term->term_id, 'hop-tac-category' );
                $count = count($term_childs);
                if($count >= 0) {
                    foreach ( $term_childs as $child ) {
                        $term = get_term_by( 'id', $child, 'hop-tac-category' );
                        if($term->parent == $team_lg){

                        if($id_danh_muc == $term->term_id) {
                            echo '<option name="'.$this->get_field_name('$term->term_id').'" selected value="'.$term->term_id.'">&nbsp;&nbsp;&nbsp;'.$term->name.'</option>';
                        }else{
                            echo '<option name="'.$this->get_field_name('$term->term_id').'" value="'.$term->term_id.'">&nbsp;&nbsp;&nbsp;'.$term->name.'</option>';
                        }
                            //
                            $term_childs = get_term_children( $term->term_id, 'hop-tac-category' );
                            $count = count($term_childs);
                            if($count >= 0) {
                                foreach ( $term_childs as $child ) {
                                    $term = get_term_by( 'id', $child, 'hop-tac-category' );

                                    if($id_danh_muc == $term->term_id) {
                                        echo '<option name="'.$this->get_field_name('$term->term_id').'" selected value="'.$term->term_id.'">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.$term->name.'</option>';
                                    }else{
                                        echo '<option name="'.$this->get_field_name('$term->term_id').'" value="'.$term->term_id.'">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.$term->name.'</option>';
                                    }
                                }
                            }
                        }
                    }
                }
            }
        echo '</select>';
        echo '<p>Số lượng bài viết hiển thị:<input type="number" class="widefat" name="'.$this->get_field_name('custom_post_number').'" value="'.$custom_post_number.'" /></p>';
    }
    function update( $new_instance, $old_instance ) {
        $instance = $old_instance;
        $instance['title'] = strip_tags($new_instance['title']);
        $instance['id_danh_muc'] = strip_tags($new_instance['id_danh_muc']);
        $instance['custom_post_number'] = strip_tags($new_instance['custom_post_number']);
        return $instance;
    }
    function widget( $args, $instance ) {
        extract($args);
        $title = apply_filters( 'widget_title', $instance['title'] );
        $id_danh_muc = $instance['id_danh_muc'];
        $custom_post_number = $instance['custom_post_number'];

        $link = get_term($id_danh_muc);

        echo $before_widget;
        echo '<div class="main-title"><a href="'.esc_url(get_term_link($link)).'"><h2>'.$title.'</h2></a></div>';
        echo '<div class="partner-content">';
            $query = tiep_custom_posttype_query('hop-tac', 'hop-tac-category', $id_danh_muc, $custom_post_number);
            if($query->have_posts()) : while ($query->have_posts() ) : $query->the_post();
            ?>
                <article class="item">
                    <figure>
                        <a href="<?php the_permalink();?>">
                            <img src="<?php echo asset('images/3x3.png'); ?>" alt="<?php the_title(); ?>" style="background-image: url(<?php echo tiep_get_thumbnail_url('full');?>);" />
                        </a>
                    </figure>
                </article>
            <?php
            endwhile; wp_reset_postdata(); endif;
        echo '</div>';
        echo $after_widget;
    }
}
function create_showpartner_widget() {
    register_widget('show_partner');
}
add_action( 'widgets_init', 'create_showpartner_widget' );
?>