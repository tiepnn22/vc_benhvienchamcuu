<?php
class info_footer extends WP_Widget {
    function __construct() {
    	parent::__construct(
            'Info_footer',
            'Footer - Liên hệ',
            array( 'description'  =>  'Footer - Liên hệ' )
        );
    }
    function form( $instance ) {
    	$default = array(
            'title' => '',
            'copyright' => '',
            'address' => '',
            'phone' => '',
            'email' => '',
            'linkphone' => '',
            'linkemail' => '',
        );
        $instance = wp_parse_args( (array) $instance, $default );
        $title = esc_attr($instance['title']);
        $copyright = esc_attr($instance['copyright']);
        $address = esc_attr($instance['address']);
        $phone = esc_attr($instance['phone']);
        $email = esc_attr($instance['email']);
        $linkphone = esc_attr($instance['linkphone']);
        $linkemail = esc_attr($instance['linkemail']);
        echo '<p>Tiêu đề:<input type="text" class="widefat" name="'.$this->get_field_name('title').'" value="'.$title.'"/></p>';
        echo '<p>Copyright:<input type="text" class="widefat" name="'.$this->get_field_name('copyright').'" value="'.$copyright.'" /></p>';
        echo '<p>Address:<input type="text" class="widefat" name="'.$this->get_field_name('address').'" value="'.$address.'" /></p>';
        echo '<p>Phone:<input type="text" class="widefat" name="'.$this->get_field_name('phone').'" value="'.$phone.'" /></p>';
        echo '<p>Email:<input type="text" class="widefat" name="'.$this->get_field_name('email').'" value="'.$email.'" /></p>';
        echo '<p>URL Phone:<input type="text" class="widefat" name="'.$this->get_field_name('linkphone').'" value="'.$linkphone.'" placeholder="viết liền số không kí tự" /></p>';
        echo '<p>URL Email:<input type="text" class="widefat" name="'.$this->get_field_name('linkemail').'" value="'.$linkemail.'" /></p>';
    }
    function update( $new_instance, $old_instance ) {
    	$instance = $old_instance;
        $instance['title'] = strip_tags($new_instance['title']);
        $instance['copyright'] = ($new_instance['copyright']);
        $instance['address'] = ($new_instance['address']);
        $instance['phone'] = ($new_instance['phone']);
        $instance['email'] = ($new_instance['email']);
        $instance['linkphone'] = ($new_instance['linkphone']);
        $instance['linkemail'] = ($new_instance['linkemail']);
        return $instance;
    }
    function widget( $args, $instance ) {
    	extract($args);
        $title = apply_filters( 'widget_title', $instance['title'] );
        $copyright = $instance['copyright'];
        $address = $instance['address'];
        $phone = $instance['phone'];
        $email = $instance['email'];
        $linkphone = $instance['linkphone'];
        $linkemail = $instance['linkemail'];

        echo $before_widget;
        echo '<div class="info-footer">';
        	echo '<h3 class="widget-title">'.$title.'</h3>';
        	echo '<div class="info-footer-content">';
	        	echo '<span><i class="fa fa-at" aria-hidden="true"></i>'.$copyright.'</span>';
	        	echo '<span><i class="fa fa-map-marker" aria-hidden="true"></i>Địa chỉ: '.$address.'</span>';
	        	echo '<span><i class="fa fa-phone" aria-hidden="true"></i>Điện thoại: <a href="tel:'.$linkphone.'">'.$phone.'</a></span>';
	        	echo '<span><i class="fa fa-envelope-o" aria-hidden="true"></i>Email: <a href="mailto:'.$linkemail.'">'.$email.'</a></span>';
        	echo '<div>';
        echo '</div>';
        echo $after_widget;
    }
}
function create_info_footer_widget() {
    register_widget('info_footer');
}
add_action( 'widgets_init', 'create_info_footer_widget' );