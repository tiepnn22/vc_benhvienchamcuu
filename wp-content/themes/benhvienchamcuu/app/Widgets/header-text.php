<?php
class text_header extends WP_Widget {
    function __construct() {
        parent::__construct(
            'Text-header',
            'Header - Text Slogan',
            array( 'description'  =>  'Header - Text Slogan' )
        );
    }
    function form( $instance ) {
        $default = array(
            'title' => '',
            'text_large' => '',
            'text_small' => '',
            'url' => '',
        );
        $instance = wp_parse_args( (array) $instance, $default );
        $title = esc_attr($instance['title']);
        $text_large = esc_attr($instance['text_large']);
        $text_small = esc_attr($instance['text_small']);
        $url = esc_attr($instance['url']);
        echo '<p>Tiêu đề:<input type="text" class="widefat" name="'.$this->get_field_name('title').'" value="'.$title.'"/></p>';
        echo '<p>Text large:<input type="text" class="widefat" name="'.$this->get_field_name('text_large').'" value="'.$text_large.'" /></p>';
        echo '<p>Text small:<input type="text" class="widefat" name="'.$this->get_field_name('text_small').'" value="'.$text_small.'" /></p>';
        echo '<p>URL text:<input type="text" class="widefat" name="'.$this->get_field_name('url').'" value="'.$url.'" /></p>';
    }
    function update( $new_instance, $old_instance ) {
        $instance = $old_instance;
        $instance['title'] = strip_tags($new_instance['title']);
        $instance['text_large'] = ($new_instance['text_large']);
        $instance['text_small'] = ($new_instance['text_small']);
        $instance['url'] = ($new_instance['url']);
        return $instance;
    }
    function widget( $args, $instance ) {
        extract($args);
        $title = apply_filters( 'widget_title', $instance['title'] );
        $text_large = $instance['text_large'];
        $text_small = $instance['text_small'];
        $url = $instance['url'];

        echo $before_widget;
            echo '<a href="'.$url.'">';
                echo '<span class="text-large">'.$text_large.'</span>';
                echo '<span class="text-small">'.$text_small.'</span>';
            echo '</a>';
        echo $after_widget;
    }
}
function create_text_header_widget() {
    register_widget('text_header');
}
add_action( 'widgets_init', 'create_text_header_widget' );