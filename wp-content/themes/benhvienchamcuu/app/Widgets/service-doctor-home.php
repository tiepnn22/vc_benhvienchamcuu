<?php
class show_post_doctor_service extends WP_Widget {
    function __construct() {
        parent::__construct(
            'show_post_doctor_service',
            'Trang chủ - Các phòng khoa',
            array( 'description'  =>  'Trang chủ - Các phòng khoa' )
        );
    }
    function form( $instance ) {
        $default = array(
            'title' => 'Các chuyên khoa',
            'id_category' => 18
        );
        $instance = wp_parse_args( (array) $instance, $default );
        $title = esc_attr($instance['title']);
        $id_category = esc_attr($instance['id_category']);

        echo '<p>Tiêu đề:<input type="text" class="widefat" name="'.$this->get_field_name('title').'" value="'.$title.'"/></p>';
        echo 'Chọn category :<select name="'.$this->get_field_name('id_category').'">';
            $categories = get_categories( );
            foreach ( $categories as $category ) {
                if($id_category == $category->term_id) {
                     echo '<option name="'.$this->get_field_name('$category->term_id').'"  selected value="'.$category->term_id.'">'.$category->name.'</option>';
                }else{
                     echo '<option name="'.$this->get_field_name('$category->term_id').'"  value="'.$category->term_id.'">'.$category->name.'</option>';
                }
            }
        echo '</select>';
    }
    function update( $new_instance, $old_instance ) {
        $instance = $old_instance;
        $instance['title'] = strip_tags($new_instance['title']);
        $instance['id_category'] = strip_tags($new_instance['id_category']);
        return $instance;
    }
    function widget( $args, $instance ) {
        extract($args);
        $title = apply_filters( 'widget_title', $instance['title'] );
        $id_category = $instance['id_category'];
        $link = get_term($id_category);

        echo $before_widget; ?>
            <section class="service">
                <div class="container">
                    <div class="main-title"><a href="<?php echo esc_url(get_term_link($link));?>"><h2><?php echo get_cat_name($id_category);?></h2></a></div>
                    <div class="service-content">
                        <?php
                            $child_cats = get_term_children( $id_category, 'category' );
                            $count = count($child_cats);
                            if($count < 1) {

                            } else {
                                foreach ($child_cats as $child) {
                                    $term = get_term_by( 'id', $child, 'category' );
                                    $term_img = types_render_termmeta( "image-cat", array('term_id' => $term->term_id , 'class' => 'img-responsive', "alt" => $term->name, "width" => "70", "height" => "70" ) );
                                    ?>
                                    <article class="item">
                                        <figure>
                                            <a class="vongtron" href="<?php echo get_term_link( get_term($term->term_id), 'category' );?>">
                                                <?php
                                                    if(!empty($term_img)) {
                                                        echo $term_img;
                                                    } else {
                                                ?>
                                                        <img src="<?php echo asset('images/3x3.png'); ?>" alt="<?php the_title(); ?>" style="background-image: url(<?php echo asset('images/no-image.jpg'); ?>);" />
                                                <?php
                                                    }
                                                ?>
                                            </a>
                                        </figure>

                                        <div class="title">
                                            <a href="<?php echo get_term_link( get_term($term->term_id), 'category' );?>">
                                                <h3><?php echo $term->name;?></h3>
                                            </a>
                                        </div>
                                    </article>
                                    <?php
                                }
                            }
                        ?>
                    </div>
                </div>
            </section>
        <?php echo $after_widget;
    }
}
function create_showpostdoctorservice_widget() {
    register_widget('show_post_doctor_service');
}
add_action( 'widgets_init', 'create_showpostdoctorservice_widget' );
?>