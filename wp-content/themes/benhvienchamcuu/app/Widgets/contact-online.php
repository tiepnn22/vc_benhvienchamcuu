<?php
class contact_online extends WP_Widget {
    function __construct() {
    	parent::__construct(
            'Contact_online',
            'Trang chủ - Hỗ trợ trực tuyến',
            array( 'description'  =>  'Trang chủ - Hỗ trợ trực tuyến' )
        );
    }
    function form( $instance ) {
    	$default = array(
            'title' => '',
            'linktitle' => '',
            'urlimg' => '',
            'linkimg' => '',
            'text1' => '',
            'text2' => '',
            'text3' => '',
            'tell1' => '',
            'tell2' => '',
            'tell3' => '',
            'link1' => '',
            'link2' => '',
            'link3' => '',
        );
        $instance = wp_parse_args( (array) $instance, $default );
        $title = esc_attr($instance['title']);
        $linktitle = esc_attr($instance['linktitle']);
        $urlimg = esc_attr($instance['urlimg']);
        $linkimg = esc_attr($instance['linkimg']);
        $text1 = esc_attr($instance['text1']);
        $text2 = esc_attr($instance['text2']);
        $text3 = esc_attr($instance['text3']);
        $tell1 = esc_attr($instance['tell1']);
        $tell2 = esc_attr($instance['tell2']);
        $tell3 = esc_attr($instance['tell3']);
        $link1 = esc_attr($instance['link1']);
        $link2 = esc_attr($instance['link2']);
        $link3 = esc_attr($instance['link3']);
        echo '<p>Tiêu đề:<input type="text" class="widefat" name="'.$this->get_field_name('title').'" value="'.$title.'"/></p>';
        echo '<p>Link tiêu đề:<input type="text" class="widefat" name="'.$this->get_field_name('linktitle').'" value="'.$linktitle.'"/></p>';
        echo '<p>URL ảnh:<input type="text" class="widefat" name="'.$this->get_field_name('urlimg').'" value="'.$urlimg.'"/></p>';
        echo '<p>Link ảnh:<input type="text" class="widefat" name="'.$this->get_field_name('linkimg').'" value="'.$linkimg.'"/></p>';
        echo '<p>Hình ảnh:<img style="width: 50px;" class="widefat" name="'.$this->get_field_name('urlimg').'" src="'.$urlimg.'"></p>';

        echo '<p>Text1:<input type="text" class="widefat" name="'.$this->get_field_name('text1').'" value="'.$text1.'" /></p>';
        echo '<p>Nội dung text1:<input type="text" class="widefat" name="'.$this->get_field_name('tell1').'" value="'.$tell1.'" /></p>';
        echo '<p>Link text1:<input type="text" class="widefat" name="'.$this->get_field_name('link1').'" value="'.$link1.'" placeholder="viết số liền không cách" /></p>';

        echo '<p>Text2:<input type="text" class="widefat" name="'.$this->get_field_name('text2').'" value="'.$text2.'" /></p>';
        echo '<p>Nội dung text2:<input type="text" class="widefat" name="'.$this->get_field_name('tell2').'" value="'.$tell2.'" /></p>';
        echo '<p>Link text2:<input type="text" class="widefat" name="'.$this->get_field_name('link2').'" value="'.$link2.'" placeholder="viết số liền không cách" /></p>';

        echo '<p>Text3:<input type="text" class="widefat" name="'.$this->get_field_name('text3').'" value="'.$text3.'" /></p>';
        echo '<p>Nội dung text3:<input type="text" class="widefat" name="'.$this->get_field_name('tell3').'" value="'.$tell3.'" /></p>';
        echo '<p>Link text3:<input type="text" class="widefat" name="'.$this->get_field_name('link3').'" value="'.$link3.'" placeholder="viết số liền không cách" /></p>';
    }
    function update( $new_instance, $old_instance ) {
    	$instance = $old_instance;
        $instance['title'] = strip_tags($new_instance['title']);
        $instance['linktitle'] = ($new_instance['linktitle']);
        $instance['urlimg'] = ($new_instance['urlimg']);
        $instance['linkimg'] = ($new_instance['linkimg']);
        $instance['text1'] = ($new_instance['text1']);
        $instance['text2'] = ($new_instance['text2']);
        $instance['text3'] = ($new_instance['text3']);
        $instance['tell1'] = ($new_instance['tell1']);
        $instance['tell2'] = ($new_instance['tell2']);
        $instance['tell3'] = ($new_instance['tell3']);
        $instance['link1'] = ($new_instance['link1']);
        $instance['link2'] = ($new_instance['link2']);
        $instance['link3'] = ($new_instance['link3']);
        return $instance;
    }
    function widget( $args, $instance ) {
    	extract($args);
        $title = apply_filters( 'widget_title', $instance['title'] );
        $linktitle = $instance['linktitle'];
        $urlimg = $instance['urlimg'];
        $linkimg = $instance['linkimg'];
        $text1 = $instance['text1'];
        $text2 = $instance['text2'];
        $text3 = $instance['text3'];
        $tell1 = $instance['tell1'];
        $tell2 = $instance['tell2'];
        $tell3 = $instance['tell3'];
        $link1 = $instance['link1'];
        $link2 = $instance['link2'];
        $link3 = $instance['link3'];

        echo $before_widget; ?>
            <div class="contact-online">
                <div class="main-title">
                    <h2>
                        <a href="<?php echo $linktitle;?>">
                            <?php echo $title;?>
                        </a>
                    </h2>
                </div>
                <div class="online-content">
                    <figure>
                        <a href="<?php echo $linkimg;?>">
                            <img src="<?php echo $urlimg; ?>" />
                        </a>
                    </figure>
                    <div class="info">
                        <span><?php echo $text1;?>: <a href="tel:<?php echo $link1;?>"><?php echo $tell1;?></a></span>
                        <span><?php echo $text2;?>: <a href="tel:<?php echo $link2;?>"><?php echo $tell2;?></a></span>
                        <span><?php echo $text3;?>: <a href="tel:<?php echo $link3;?>"><?php echo $tell3;?></a></span>
                    </div>
                </div>
            </div>
        <?php echo $after_widget;
    }
}
function create_contact_online_widget() {
    register_widget('contact_online');
}
add_action( 'widgets_init', 'create_contact_online_widget' );