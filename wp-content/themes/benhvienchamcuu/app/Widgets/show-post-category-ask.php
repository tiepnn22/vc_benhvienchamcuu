<?php
class show_post_category_ask extends WP_Widget {
    function __construct() {
        parent::__construct(
            'show_post_category_ask',
            'Sidebar - Hỏi đáp',
            array( 'description'  =>  'Sidebar - Hỏi đáp' )
        );
    }
    function form( $instance ) {
        $default = array(
            'title' => 'Hỏi đáp',
            'id_category' => 8,
            'number_post' => 4
        );
        $instance = wp_parse_args( (array) $instance, $default );
        $title = esc_attr($instance['title']);
        $id_category = esc_attr($instance['id_category']);
        $number_post = esc_attr($instance['number_post']);

        echo '<p>Tiêu đề:<input type="text" class="widefat" name="'.$this->get_field_name('title').'" value="'.$title.'"/></p>';
        echo 'Chọn category :<select name="'.$this->get_field_name('id_category').'">';

            $categories = get_categories( array(
                'orderby' => 'name',
                'parent'  => 0
            ) );

            foreach ( $categories as $category ) {
                if($category->term_id == 1) {} else {
                    if($id_category == $category->term_id) {
                        echo '<option name="'.$this->get_field_name('$category->term_id').'" selected value="'.$category->term_id.'">'.$category->name.'</option>';
                    }else{
                        echo '<option name="'.$this->get_field_name('$category->term_id').'" value="'.$category->term_id.'">'.$category->name.'</option>';
                    }
                }
                $team_lg = $category->term_id;

                $child_cats = get_term_children( $category->term_id, 'category' );
                foreach ($child_cats as $childs) {
                    $child_cat = get_term_by( 'id', $childs, 'category' );

                    if($child_cat->parent == $team_lg){

                        if($id_category == $child_cat->term_id) {
                            echo '<option name="'.$this->get_field_name('$child_cat->term_id').'" selected value="'.$child_cat->term_id.'">&nbsp;&nbsp;&nbsp;'.$child_cat->name.'</option>';
                        }else{
                            echo '<option name="'.$this->get_field_name('$child_cat->term_id').'" value="'.$child_cat->term_id.'">&nbsp;&nbsp;&nbsp;'.$child_cat->name.'</option>';
                        }

                        $child_cats = get_term_children( $child_cat->term_id, 'category' );
                        foreach ( $child_cats as $childs ) {
                            $child_cat = get_term_by( 'id', $childs, 'category' );

                            if($id_category == $child_cat->term_id) {
                                echo '<option name="'.$this->get_field_name('$child_cat->term_id').'" selected value="'.$child_cat->term_id.'">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.$child_cat->name.'</option>';
                            }else{
                                echo '<option name="'.$this->get_field_name('$child_cat->term_id').'" value="'.$child_cat->term_id.'">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.$child_cat->name.'</option>';
                            }
                        }
                    }
                }
            }
        echo '</select>';
        echo '<p>Số lượng bài viết hiển thị:<input type="number" class="widefat" name="'.$this->get_field_name('number_post').'" value="'.$number_post.'" /></p>';
    }
    function update( $new_instance, $old_instance ) {
        $instance = $old_instance;
        $instance['title'] = strip_tags($new_instance['title']);
        $instance['id_category'] = strip_tags($new_instance['id_category']);
        $instance['number_post'] = strip_tags($new_instance['number_post']);
        return $instance;
    }
    function widget( $args, $instance ) {
        extract($args);
        $title = apply_filters( 'widget_title', $instance['title'] );
        $id_category = $instance['id_category'];
        $number_post = $instance['number_post'];
        $link = get_term($id_category);

        echo $before_widget; ?>
            <div class="news related-post">
                <div class="main-title">
                    <h2>
                        <a href="<?php echo esc_url(get_term_link($link));?>">
                            <?php echo get_cat_name($id_category);?>
                        </a>
                    </h2>
                </div>
                <div class="menu-menu-hoi-dap-sidebar-container">

                    <ul>
                        <?php
                            $query = new WP_Query(array('cat'=>$id_category,'showposts'=>$number_post,'order' => 'DESC','orderby' => 'date'));
                            if($query->have_posts()) : while ($query->have_posts() ) : $query->the_post();
                        ?>

                        <li>
                            <a href="<?php the_permalink();?>" rel="bookmark">
                                <h3><?php the_title();?></h3>
                            </a>
                        </li>

                        <?php endwhile; wp_reset_query(); else: echo ''; endif; ?>
                    </ul>

                    <a class="btn-more-detail" href="<?php echo esc_url(get_term_link($link));?>">Xem thêm</a>

                </div>
            </div>
        <?php echo $after_widget;
    }
}
function create_showpostcategoryask_widget() {
    register_widget('show_post_category_ask');
}
add_action( 'widgets_init', 'create_showpostcategoryask_widget' );
?>