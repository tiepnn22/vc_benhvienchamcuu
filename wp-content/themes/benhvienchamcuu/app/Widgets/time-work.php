<?php
class time_work extends WP_Widget {
    function __construct() {
        parent::__construct(
            'Time-work',
            'Sidebar - Giờ làm việc',
            array( 'description'  =>  'Sidebar - Giờ làm việc' )
        );
    }
    function form( $instance ) {
        $default = array(
            'title' => '',
            'text1' => '',
            'time' => '',
            'hospital' => '',
            'text2' => '',
        );
        $instance = wp_parse_args( (array) $instance, $default );
        $title = esc_attr($instance['title']);
        $text1 = esc_attr($instance['text1']);
        $time = esc_attr($instance['time']);
        $hospital = esc_attr($instance['hospital']);
        $text2 = esc_attr($instance['text2']);

        echo '<p>Tiêu đề:<input type="text" class="widefat" name="'.$this->get_field_name('title').'" value="'.$title.'"/></p>';
        echo '<p>Text 1:<input type="text" class="widefat" name="'.$this->get_field_name('text1').'" value="'.$text1.'" /></p>';
        echo '<p>Thời gian:<input type="text" class="widefat" name="'.$this->get_field_name('time').'" value="'.$time.'" /></p>';
        echo '<p>Trực cấp cứu:<input type="text" class="widefat" name="'.$this->get_field_name('hospital').'" value="'.$hospital.'" /></p>';
        echo '<p>Text 2:<input type="text" class="widefat" name="'.$this->get_field_name('text2').'" value="'.$text2.'" /></p>';
    }
    function update( $new_instance, $old_instance ) {
        $instance = $old_instance;
        $instance['title'] = strip_tags($new_instance['title']);
        $instance['text1'] = ($new_instance['text1']);
        $instance['time'] = ($new_instance['time']);
        $instance['hospital'] = ($new_instance['hospital']);
        $instance['text2'] = ($new_instance['text2']);
        return $instance;
    }
    function widget( $args, $instance ) {
        extract($args);
        $title = apply_filters( 'widget_title', $instance['title'] );
        $text1 = $instance['text1'];
        $time = $instance['time'];
        $hospital = $instance['hospital'];
        $text2 = $instance['text2'];

        echo $before_widget; ?>
            <div class="time-work">
                <h2 class="main-title widget-title"><a><?php echo $title; ?></a></h2>
                <div class="time-work-content">
                    <span><?php echo $text1; ?></span>
                    <span>Thời gian: <span class="text"><?php echo $time; ?></span></span>
                    <span>Trực cấp cứu: <span class="text"><?php echo $hospital; ?></span></span>
                    <span><?php echo $text2; ?></span>
                </div>
            </div>
        <?php echo $after_widget;
    }
}
function create_time_work_widget() {
    register_widget('time_work');
}
add_action( 'widgets_init', 'create_time_work_widget' );