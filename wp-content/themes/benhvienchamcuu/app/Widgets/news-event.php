<?php
class show_post_news_event extends WP_Widget {
    function __construct() {
        parent::__construct(
            'show_post_news_event',
            'Tin nổi bật trang chi tiết',
            array( 'description'  =>  'Tin nổi bật trang chi tiết' )
        );
    }
    function form( $instance ) {
        $default = array(
            'title' => 'Tin nổi bật',
            'id_category' => 24,
            'number_post' => 4
        );
        $instance = wp_parse_args( (array) $instance, $default );
        $title = esc_attr($instance['title']);
        $id_category = esc_attr($instance['id_category']);
        $number_post = esc_attr($instance['number_post']);

        echo '<p>Tiêu đề:<input type="text" class="widefat" name="'.$this->get_field_name('title').'" value="'.$title.'"/></p>';
        echo 'Chọn category :<select name="'.$this->get_field_name('id_category').'">';
            $categories = get_categories( );
            foreach ( $categories as $category ) {
                if($id_category == $category->term_id) {
                     echo '<option name="'.$this->get_field_name('$category->term_id').'"  selected value="'.$category->term_id.'">'.$category->name.'</option>';
                }else{
                     echo '<option name="'.$this->get_field_name('$category->term_id').'"  value="'.$category->term_id.'">'.$category->name.'</option>';
                }
            }
        echo '</select>';
        echo '<p>Số lượng bài viết hiển thị:<input type="number" class="widefat" name="'.$this->get_field_name('number_post').'" value="'.$number_post.'" /></p>';
    }
    function update( $new_instance, $old_instance ) {
        $instance = $old_instance;
        $instance['title'] = strip_tags($new_instance['title']);
        $instance['id_category'] = strip_tags($new_instance['id_category']);
        $instance['number_post'] = strip_tags($new_instance['number_post']);
        return $instance;
    }
    function widget( $args, $instance ) {
        extract($args);
        $title = apply_filters( 'widget_title', $instance['title'] );
        $id_category = $instance['id_category'];
        $number_post = $instance['number_post'];
        $link = get_term($id_category);

        echo $before_widget; ?>
            <div class="news related-post">
                <div class="main-title">
                    <a href="<?php echo esc_url(get_term_link($link));?>">
                        <h2><?php echo get_cat_name($id_category);?></h2>
                    </a>
                </div>
                <div class="news-content">
                    <div class="row">
                        <?php
                            $query = new WP_Query(array('cat'=>$id_category,'showposts'=>$number_post,'order' => 'DESC','orderby' => 'date'));
                            if($query->have_posts()) : while ($query->have_posts() ) : $query->the_post();
                        ?>
                    <article class="col-lg-3 col-md-3 col-sm-6 col-xs-6 item">
                        <figure>
                            <a href="<?php the_permalink();?>">
                                <img src="<?php echo asset('images/3x2.png'); ?>" alt="<?php the_title(); ?>" style="background-image: url(<?php echo tiep_get_thumbnail_url('news');?>);" />
                            </a>
                        </figure>
                        <div class="info">
                            <div class="title">
                                <a href="<?php the_permalink();?>" rel="bookmark">
                                    <h3><?php the_title();?></h3>
                                </a>
                            </div>
                            <div class="desc">
                                <?php echo cut_string(get_the_excerpt(),120,'...');?>
                            </div>
                            <a href="<?php the_permalink();?>" rel="bookmark" class="btn-more">Xem thêm >></a>
                        </div>
                    </article>
                        <?php endwhile; wp_reset_query(); else: echo ''; endif; ?>
                    </div>
                </div>
            </div>
        <?php echo $after_widget;
    }
}
function create_showpostnewsevent_widget() {
    register_widget('show_post_news_event');
}
add_action( 'widgets_init', 'create_showpostnewsevent_widget' );
?>