<?php
/**
 * Enqueue scripts and stylesheet
 */
add_action('wp_enqueue_scripts', 'theme_enqueue_scripts');
add_action('wp_enqueue_scripts', 'theme_enqueue_style');


function theme_enqueue_style()
{
    wp_enqueue_style(
        'template-style',
        get_stylesheet_directory_uri() . '/dist/styles/main.css',
        false
    );
}

function theme_enqueue_scripts()
{
    wp_enqueue_script(
        'template-scripts',
        get_stylesheet_directory_uri() . '/dist/scripts/main.js',
        'jquery',
        '1.0',
        true
    );
}

//lay duong dan
if (!function_exists('asset')) {
    function asset($path)
    {
        return wp_slash(get_stylesheet_directory_uri() . '/dist/' . $path);
    }
}

//shortcode có thể chạy được trong widget
add_filter('widget_text','do_shortcode');

//đăng ký menu và img
if (!function_exists('chamcuuSetup')) {
    /**
     * setup support for theme
     *
     * @return void
     */
    function chamcuuSetup()
    {
        // Register menus
        register_nav_menus( array(
            'primary' => __( 'Menu chính', 'chamcuu' ),
            'service-footer' => __( 'Menu dịch vụ y khoa footer', 'chamcuu' ),
            // 'introduce-sidebar' => __( 'Menu giới thiệu sidebar', 'chamcuu' ),
            // 'ask-sidebar' => __( 'Menu hỏi đáp sidebar', 'chamcuu' ),
            // 'guide-sidebar' => __( 'Menu hướng dẫn sidebar', 'chamcuu' ),
        ) );

        // add_theme_support('menus');
        add_theme_support('post-thumbnails');
        add_image_size('doctor', 88, 88, true);
        add_image_size('news', 160, 94, true);
        add_image_size('news-sidebar', 130, 88, true);
    }

    add_action('after_setup_theme', 'chamcuuSetup');
}


//xoa class va id wp_nav_menu()
function wp_nav_menu_attributes_filter($var) {
    return is_array($var) ? array_intersect($var, array('current-menu-item','mega-menu')) : '';
}
add_filter('nav_menu_css_class', 'wp_nav_menu_attributes_filter', 100, 1);
add_filter('nav_menu_item_id', 'wp_nav_menu_attributes_filter', 100, 1);
add_filter('page_css_class', 'wp_nav_menu_attributes_filter', 100, 1);


//dang ky sidebar
if (!function_exists('chamcuuWidgets')) {
    /**
     * register sidebar for theme
     *
     * @return void
     */
    function chamcuuWidgets()
    {
        $sidebars = [
            [
                'name'          => __('Header - Slogan', 'chamcuu'),
                'id'            => 'header-text',
                'description'   => __('Header - Slogan', 'chamcuu'),
                'before_widget' => '<section id="%1$s" class="widget %2$s">',
                'after_widget'  => '</section>',
                'before_title'  => '<h2 class="widget-title">',
                'after_title'   => '</h2>',
            ],
            [
                'name'          => __('Header - Liên hệ', 'chamcuu'),
                'id'            => 'header-contact',
                'description'   => __('Header - Liên hệ', 'chamcuu'),
                'before_widget' => '<section id="%1$s" class="widget %2$s">',
                'after_widget'  => '</section>',
                'before_title'  => '<h2 class="widget-title">',
                'after_title'   => '</h2>',
            ],
            [
                'name'          => __('Footer - Liên hệ', 'chamcuu'),
                'id'            => 'footer-2',
                'description'   => __('Footer - Liên hệ', 'chamcuu'),
                'before_widget' => '<section id="%1$s" class="widget %2$s">',
                'after_widget'  => '</section>',
                'before_title'  => '<h2 class="widget-title">',
                'after_title'   => '</h2>',
            ],
            [
                'name'          => __('Footer - Bản đồ', 'chamcuu'),
                'id'            => 'footer-map',
                'description'   => __('Footer - Bản đồ', 'chamcuu'),
                'before_widget' => '<section id="%1$s" class="widget %2$s">',
                'after_widget'  => '</section>',
                'before_title'  => '<h2 class="widget-title">',
                'after_title'   => '</h2>',
            ],
            [
                'name'          => __('Footer - Menu dịch vụ y khoa', 'chamcuu'),
                'id'            => 'footer-3',
                'description'   => __('Footer - Menu dịch vụ y khoa', 'chamcuu'),
                'before_widget' => '<section id="%1$s" class="widget %2$s">',
                'after_widget'  => '</section>',
                'before_title'  => '<h2 class="widget-title">',
                'after_title'   => '</h2>',
            ],
            // [
            //     'name'          => __('Trang chủ - Hỗ trợ trực tuyến', 'chamcuu'),
            //     'id'            => 'contact-online',
            //     'description'   => __('Trang chủ - Hỗ trợ trực tuyến', 'chamcuu'),
            //     'before_widget' => '<section id="%1$s" class="widget %2$s">',
            //     'after_widget'  => '</section>',
            //     'before_title'  => '<h2 class="widget-title">',
            //     'after_title'   => '</h2>',
            // ],
            [
                'name'          => __('Trang chủ - Slide', 'chamcuu'),
                'id'            => 'home-slide',
                'description'   => __('Trang chủ - Slide', 'chamcuu'),
                'before_widget' => '<section id="%1$s" class="widget %2$s">',
                'after_widget'  => '</section>',
                'before_title'  => '<h2 class="widget-title">',
                'after_title'   => '</h2>',
            ],
            [
                'name'          => __('Trang chủ - Các phòng khoa', 'chamcuu'),
                'id'            => 'home-service',
                'description'   => __('Trang chủ - Các phòng khoa', 'chamcuu'),
                'before_widget' => '<section id="%1$s" class="widget %2$s">',
                'after_widget'  => '</section>',
                'before_title'  => '<h2 class="widget-title">',
                'after_title'   => '</h2>',
            ],
            [
                'name'          => __('Trang chủ - Đội ngũ chuyên gia', 'chamcuu'),
                'id'            => 'home-doctor',
                'description'   => __('Trang chủ - Đội ngũ chuyên gia', 'chamcuu'),
                'before_widget' => '<section id="%1$s" class="widget %2$s">',
                'after_widget'  => '</section>',
                'before_title'  => '<h2 class="widget-title">',
                'after_title'   => '</h2>',
            ],
            [
                'name'          => __('Trang chủ - Tin tức và sự kiện', 'chamcuu'),
                'id'            => 'home-news',
                'description'   => __('Trang chủ - Tin tức và sự kiện', 'chamcuu'),
                'before_widget' => '<section id="%1$s" class="widget %2$s">',
                'after_widget'  => '</section>',
                'before_title'  => '<h2 class="widget-title">',
                'after_title'   => '</h2>',
            ],
            [
                'name'          => __('Trang chủ - Hợp tác chuyên môn', 'chamcuu'),
                'id'            => 'home-partner',
                'description'   => __('Trang chủ - Hợp tác chuyên môn', 'chamcuu'),
                'before_widget' => '<section id="%1$s" class="widget %2$s">',
                'after_widget'  => '</section>',
                'before_title'  => '<h2 class="widget-title">',
                'after_title'   => '</h2>',
            ],
            [
                'name'          => __('Tin nổi bật trang chi tiết', 'chamcuu'),
                'id'            => 'news-event',
                'description'   => __('Tin nổi bật trang chi tiết', 'chamcuu'),
                'before_widget' => '<section id="%1$s" class="widget %2$s">',
                'after_widget'  => '</section>',
                'before_title'  => '<h2 class="widget-title">',
                'after_title'   => '</h2>',
            ],



            [
                'name'          => __('Sidebar Trang chủ', 'chamcuu'),
                'id'            => 'sidebar-home',
                'description'   => __('This is sidebar for home', 'chamcuu'),
                'before_widget' => '<section id="%1$s" class="widget %2$s">',
                'after_widget'  => '</section>',
                'before_title'  => '<h2 class="main-title widget-title"><a>',
                'after_title'   => '</a></h2>',
            ],
            [
                'name'          => __('Sidebar Trang danh sách bài tin', 'chamcuu'),
                'id'            => 'sidebar-category',
                'description'   => __('This is sidebar for category', 'chamcuu'),
                'before_widget' => '<section id="%1$s" class="widget %2$s">',
                'after_widget'  => '</section>',
                'before_title'  => '<h2 class="main-title widget-title"><a>',
                'after_title'   => '</a></h2>',
            ],
            [
                'name'          => __('Sidebar Trang danh sách bài tin bác sĩ', 'chamcuu'),
                'id'            => 'sidebar-taxonomy',
                'description'   => __('This is sidebar for taxonomy + archive', 'chamcuu'),
                'before_widget' => '<section id="%1$s" class="widget %2$s">',
                'after_widget'  => '</section>',
                'before_title'  => '<h2 class="main-title widget-title"><a>',
                'after_title'   => '</a></h2>',
            ],
            [
                'name'          => __('Sidebar Trang liên hệ', 'chamcuu'),
                'id'            => 'sidebar-contact',
                'description'   => __('This is sidebar for contact + page + page template', 'chamcuu'),
                'before_widget' => '<section id="%1$s" class="widget %2$s">',
                'after_widget'  => '</section>',
                'before_title'  => '<h2 class="main-title widget-title"><a>',
                'after_title'   => '</a></h2>',
            ],
            [
                'name'          => __('Sidebar Trang chi tiết', 'chamcuu'),
                'id'            => 'sidebar-singular',
                'description'   => __('This is sidebar for singular', 'chamcuu'),
                'before_widget' => '<section id="%1$s" class="widget %2$s">',
                'after_widget'  => '</section>',
                'before_title'  => '<h2 class="main-title widget-title"><a>',
                'after_title'   => '</a></h2>',
            ],
            [
                'name'          => __('Sidebar Trang còn lại', 'chamcuu'),
                'id'            => 'sidebar-remain',
                'description'   => __('This is sidebar for search + 404 + remain', 'chamcuu'),
                'before_widget' => '<section id="%1$s" class="widget %2$s">',
                'after_widget'  => '</section>',
                'before_title'  => '<h2 class="main-title widget-title"><a>',
                'after_title'   => '</a></h2>',
            ],
        ];

        foreach ($sidebars as $sidebar) {
            register_sidebar($sidebar);
        }
    }
    add_action('widgets_init', 'chamcuuWidgets');
}
