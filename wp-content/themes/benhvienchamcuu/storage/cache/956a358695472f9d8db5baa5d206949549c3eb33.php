<?php
	use NF\View\Facades\View;
	echo View::render('partials.home-slide');
?>


<section class="page-list">
	<div class="container">
		<div class="row">
			<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12 page">

				<?php if(have_posts()) : while (have_posts() ) : the_post(); ?>
					<div class="main-title">
						<a>
							<h1><?php the_title(); ?></h1>
						</a>
					</div>
					<div class="page-content">
						<div class="page-info">
							<?php the_content(); ?>
						</div>
					</div>
				<?php endwhile; wp_reset_query(); else: echo ''; endif; ?>

			</div>

			<?php
				echo View::render('partials.sidebar');
			?>
		</div>
	</div>
</section>