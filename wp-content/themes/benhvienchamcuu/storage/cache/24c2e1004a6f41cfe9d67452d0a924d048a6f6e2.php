<?php
	use NF\View\Facades\View;
	echo View::render('partials.home-slide');
?>

<section class="download">
	<div class="container">
		<div class="row">
			<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12 download-content">
				<?php echo do_shortcode('[wpdm_category id="download"]'); ?>
			</div>

			<?php
				echo View::render('partials.sidebar');
			?>
		</div>
	</div>
</section>