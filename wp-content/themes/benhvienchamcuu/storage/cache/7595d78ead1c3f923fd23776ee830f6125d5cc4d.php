<?php
	global $post;
	$terms = get_the_terms( $post->ID , 'chuyen-khoa-category', 'string');
	$term_ids = wp_list_pluck($terms,'term_id');
	$query = new WP_Query( array(
			'post_type' => 'chuyen-khoa',
			'tax_query' => array(
								array(
										'taxonomy' => 'chuyen-khoa-category',
										'field' => 'id',
										'terms' => $term_ids,
										'operator'=> 'IN'
								 )),
			'posts_per_page' => 4,
			'orderby' => 'date',
			'post__not_in'=>array($post->ID)
	 ) );
?>
<aside class="team-doctor related-chuyen-khoa">
	<div class="main-title"><a><h2>Chuyên gia liên quan</h2></a></div>
	<div class="doctor-content">
		<div class="row">

		<?php if($query->have_posts()) : while($query->have_posts()) : $query->the_post(); ?>
			<article class="col-lg-3 col-md-3 col-sm-6 col-xs-6 item">
				<span class="doctor-content-item">
					<figure>
						<a class="vongtron" href="<?php the_permalink();?>">
							<img src="<?php echo asset('images/3x3.png'); ?>" alt="<?php the_title(); ?>" style="background-image: url(<?php echo tiep_get_thumbnail_url('doctor');?>);" />
						</a>
					</figure>
					<div class="info">
						<div class="title">
							<a href="<?php the_permalink();?>">
								<span><?php echo types_render_field( "chuc-vu", array("output"=>"raw") );?></span>
								<h3><?php the_title();?></h3>
							</a>
						</div>
						<div class="desc">
							<?php echo cut_string(get_the_excerpt(),120,'...');?>
						</div>
					</div>
				</span>
			</article>
		<?php endwhile; wp_reset_query(); else: echo ''; endif; ?>

		</div>
	</div>
</aside>