<?php
	use NF\View\Facades\View;
	echo View::render('partials.home-slide');
	echo View::render('partials.service-home');
?>

<section class="home-main">
	<div class="container">
		<div class="row">
			<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12 page-content">


				<?php
					// dynamic_sidebar( 'contact-online' );
				?>

				<?php
					dynamic_sidebar( 'home-doctor' );
				?>

				<?php
					dynamic_sidebar( 'home-news' );
				?>


			</div>

			<?php
				echo View::render('partials.sidebar');
			?>
		</div>
	</div>
</section>