<?php
	// $current_category = get_category_by_slug( get_query_var( 'category_name' ) );
	// $id = $current_category->term_id;
	// $term_img = types_render_termmeta( "image-cat", array('term_id' => $id , 'class' => 'img-responsive') );
?>
<!-- <section class="banner"> -->
	<?php
		// echo $term_img;
	?>
<!-- </section> -->


<?php
	use NF\View\Facades\View;
	echo View::render('partials.home-slide');
	echo View::render('partials.service-home');
?>

<section class="news-list">
	<div class="container">
		<div class="row">
			<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12 news">
				<div class="main-title">
					<a>
						<h2><?php echo single_cat_title( '', false ); ?></h2>
					</a>
				</div>
				<div class="news-content">
					<div class="row">
						<?php if(have_posts()) : while (have_posts() ) : the_post(); ?>
							<article class="col-lg-6 col-md-6 col-sm-6 col-xs-6 item">
								<figure>
									<a href="<?php the_permalink();?>">
										<img src="<?php echo asset('images/3x2.png'); ?>" alt="<?php the_title(); ?>" style="background-image: url(<?php echo tiep_get_thumbnail_url('news');?>);" />
									</a>
								</figure>
								<div class="info">
									<div class="title">
										<a href="<?php the_permalink();?>">
											<h3><?php the_title();?></h3>
										</a>
									</div>
									<div class="desc">
										<?php echo cut_string(get_the_excerpt(),120,'...');?>
									</div>
									<a href="<?php the_permalink();?>" class="btn-more">Xem thêm >></a>
								</div>
							</article>
						<?php endwhile; wp_reset_query(); else: echo ''; endif; ?>
					</div>
				</div>
				<nav class="navigation">
					<?php wp_pagenavi(); ?>
				</nav>
			</div>
			<?php
				echo View::render('partials.sidebar');
			?>
		</div>
	</div>
</section>
