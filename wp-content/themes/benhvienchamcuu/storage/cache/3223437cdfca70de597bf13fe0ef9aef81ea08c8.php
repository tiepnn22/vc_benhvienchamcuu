<?php
	$terms = get_terms('hop-tac-category', array(
        'parent'=> 0,
        'hide_empty' => false
    ) );
    foreach($terms as $term){
        $doctor_name = $term->name;
        $doctor_id = $term->term_id;
    }
    $term_img = types_render_termmeta( "image-cat", array('term_id' => $term->term_id , 'class' => 'img-responsive') );
?>
<section class="banner"><?php echo $term_img; ?></section>


<section class="p-list list-hop-tac">
	<div class="container">
		<div class="row">
			<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12 team-doctor">
				<div class="main-title">
					<a href="<?php echo esc_url(get_term_link($doctor_id));?>"><h2><?php echo $doctor_name;?></h2></a>
				</div>
				<div class="doctor-content">
					<?php
						$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
						$query = tiep_custom_posttype_query_paged('hop-tac', 'hop-tac-category', $doctor_id, $paged, 10);
						if($query->have_posts()) : while ($query->have_posts() ) : $query->the_post();
					?>
						<article class="item">
							<span class="doctor-content-item">
								<figure><a class="vongtron" href="<?php the_permalink();?>"><img src="<?php echo tiep_get_thumbnail_url('doctor'); ?>" alt="<?php the_title(); ?>" /></a></figure>
								<div class="info">
									<div class="title"><a href="<?php the_permalink();?>">
										<span><?php echo types_render_field( "chuc-vu", array("output"=>"raw") );?></span>
										<h3><?php the_title();?></h3></a></div>
									<div class="desc"><?php echo cut_string(get_the_excerpt(),500,'...');?></div>
								</div>
							</span>
						</article>
					<?php endwhile; wp_reset_postdata(); endif; ?>
				</div>
				<nav class="navigation">
					<?php wp_pagenavi(array( 'query' => $query )); ?>
				</nav>
			</div>


			<?php
				use NF\View\Facades\View;
				echo View::render('partials.sidebar');
				echo View::render('partials.facebook-chat');
			?>
		</div>
	</div>
</section>
