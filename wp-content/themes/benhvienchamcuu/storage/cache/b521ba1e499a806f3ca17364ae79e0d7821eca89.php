<?php
	$term = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) );
	$doctor_id = $term->term_id;
	$doctor_name = $term->name;

	// $term_img = types_render_termmeta( "image-cat", array('term_id' => $doctor_id , 'class' => 'img-responsive') );
?>
<!-- <section class="banner"> -->
	<?php
		// echo $term_img;
	?>
<!-- </section> -->

<?php
	use NF\View\Facades\View;
	echo View::render('partials.home-slide');
?>

<section class="p-list list-chuyen-khoa">
	<div class="container">
		<div class="row">
			<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12 team-doctor">
				<div class="main-title">
					<a href="<?php echo esc_url(get_term_link($doctor_id));?>">
						<h2><?php echo $doctor_name;?></h2>
					</a>
				</div>
				<div class="doctor-content">
					<?php
						$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
						$query = tiep_custom_posttype_query_paged('chuyen-khoa', 'chuyen-khoa-category', $doctor_id, $paged, 10);
						if($query->have_posts()) : while ($query->have_posts() ) : $query->the_post();
					?>
						<article class="item">
							<span class="doctor-content-item">
								<figure>
									<a class="vongtron" href="<?php the_permalink();?>">
										<img src="<?php echo asset('images/3x3.png'); ?>" alt="<?php the_title(); ?>" style="background-image: url(<?php echo tiep_get_thumbnail_url('doctor');?>);" />
									</a>
								</figure>
								<div class="info">
									<div class="title">
										<a href="<?php the_permalink();?>">
											<span><?php echo types_render_field( "chuc-vu", array("output"=>"raw") );?></span>
											<h3><?php the_title();?></h3>
										</a>
									</div>
									<div class="desc">
										<?php echo cut_string(get_the_excerpt(),500,'...');?>
									</div>
								</div>
							</span>
						</article>
					<?php endwhile; wp_reset_postdata(); endif; ?>
				</div>
				<nav class="navigation">
					<?php wp_pagenavi(array( 'query' => $query )); ?>
				</nav>
			</div>


			<?php
				echo View::render('partials.sidebar');
			?>
		</div>
	</div>
</section>
