<header class="header">
	<div class="header-top">
		<div class="container">
			<div class="logo">
				<?php
                    if ( function_exists( 'the_custom_logo' ) ) {
					    the_custom_logo();
					}
	            ?>
			</div>
			<div class="content-header">
				<div class="content-header-top">
					<?php dynamic_sidebar( 'header-text' ); ?>
				</div>
				<div class="content-header-bottom">
					<?php dynamic_sidebar( 'header-contact' ); ?>
				</div>
			</div>
		</div>
	</div>

	<div class="header-bottom">
		<div class="container">
			<nav class="menu">
				<div class="search-box">
		            <form action="<?php echo esc_url( home_url( '/' ) ); ?>">
		                <input type="text" placeholder="Tìm kiếm..." name="s" value="<?php echo get_search_query(); ?>">
		                <button type="submit" class="search-icon"></button>
		            </form>
		        </div>
				<div class="main-menu">
					<?php
		                if(function_exists('wp_nav_menu')){
		                    $args = array(
		                        'theme_location' => 'primary',
		                        'link_before'=>'',
		                        'link_after'=>'',
		                        'container_class'=>'',
		                        'menu_class'=>'menu-primary',
		                        'menu_id'=>'',
		                        'container'=>'ul',
		                        'before'=>'',
		                        'after'=>''
		                    );
		                    wp_nav_menu( $args );
		                }
		            ?>
				</div>
				<div class="mobile-menu"></div>
			</nav>
		</div>
	</div>
</header>
