<!-- <section class="banner"> -->
	<?php
		// if(!empty($banner)) {
		// 	echo '<img src="'.$banner.'">';
		// }
	?>
<!-- </section> -->


<?php
    use NF\View\Facades\View;
	echo View::render('partials.home-slide');
?>


<section class="page-contact">
	<div class="container">
		<div class="row">
			<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12 page-contact-content">
				<?php if(have_posts()) : while(have_posts()) : the_post(); ?>
					<div class="main-title">
						<a>
							<h1><?php the_title(); ?></h1>
						</a>
					</div>
					<div class="single-title">
						<h2><?php echo get_option('blogname');?></h2>
					</div>
					<div class="page-contact-info">
						<div class="page-info">
							<?php the_content(); ?>
						</div>
					</div>
                <?php endwhile; wp_reset_query(); endif; ?>
			</div>

			<?php
				echo View::render('partials.sidebar');
			?>
		</div>
	</div>
</section>