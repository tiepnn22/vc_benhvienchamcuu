<?php dynamic_sidebar( 'home-slide' ); ?>

<?php
	use NF\View\Facades\View;
	echo View::render('partials.service-home');
?>

<section class="home-main">
	<div class="container">
		<div class="row">
			<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12 page-content">


				<?php dynamic_sidebar( 'contact-online' ); ?>


				<div class="team-doctor">
					<?php
						$terms = get_terms('chuyen-khoa-category', array(
			                'parent'=> 0,
			                'hide_empty' => false
			            ) );
			            foreach($terms as $term){
			                $doctor_name = $term->name;
			                $doctor_id = $term->term_id;
			            }
					?>
					<div class="main-title"><a href="<?php echo esc_url(get_term_link($doctor_id));?>"><h2><?php echo $doctor_name;?></h2></a></div>
					<div class="doctor-content">
						<div class="row">
							<?php
								$query = tiep_custom_posttype_query('chuyen-khoa', 'chuyen-khoa-category', $doctor_id, 4);
								if($query->have_posts()) : while ($query->have_posts() ) : $query->the_post();
							?>
								<article class="col-lg-3 col-md-3 col-sm-6 col-xs-6 item">
									<span class="doctor-content-item">
										<figure><a class="vongtron" href="<?php the_permalink();?>"><img src="<?php echo tiep_get_thumbnail_url('doctor'); ?>" alt="<?php the_title(); ?>" /></a></figure>
										<div class="info">
											<div class="title"><a href="<?php the_permalink();?>">
												<span><?php echo types_render_field( "chuc-vu", array("output"=>"raw") );?></span>
												<h3><?php the_title();?></h3></a></div>
											<div class="desc"><?php echo cut_string(get_the_excerpt(),120,'...');?></div>
										</div>
									</span>
								</article>
							<?php endwhile; wp_reset_postdata(); else: echo ''; endif; ?>
						</div>
					</div>
				</div>


				<?php dynamic_sidebar( 'home-news' ); ?>


			</div>

			<?php
				echo View::render('partials.sidebar');
				echo View::render('partials.facebook-chat');
			?>
		</div>
	</div>
</section>