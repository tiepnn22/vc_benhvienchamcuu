<aside class="col-lg-4 col-md-4 col-sm-4 col-xs-12 sidebar">

	<?php
		if ( is_home() ) {
		    dynamic_sidebar( 'sidebar-home' );
		} else {
			if ( is_category() ) {
				dynamic_sidebar( 'sidebar-category' );
			} else {
				if ( is_tax() || is_archive() ) {
				    dynamic_sidebar( 'sidebar-taxonomy' );
				} else {
					if ( is_page() || is_page_template() ) {
						dynamic_sidebar( 'sidebar-contact' );
					} else {
						if ( is_singular() ) {
							dynamic_sidebar( 'sidebar-singular' );
						} else {
							dynamic_sidebar( 'sidebar-remain' );
						}
					}
				}
			}
		}
	?>

</aside>