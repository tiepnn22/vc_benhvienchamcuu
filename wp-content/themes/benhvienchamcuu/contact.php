<?php
/**
 * Template Name: Trang liên hệ
 */

get_header();

use NF\View\Facades\View;

$pageId = get_the_ID();

$banner = wp_get_attachment_image_src(get_post_thumbnail_id($pageId), 'full');

$data = [
	'banner' => $banner[0]
];

echo View::render('partials.contact', $data);

get_footer();