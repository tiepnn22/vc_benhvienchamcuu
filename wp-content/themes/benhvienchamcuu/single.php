<?php get_header(); ?>
<?php
	use NF\View\Facades\View;
	echo View::render('partials.single');
?>
<?php get_footer(); ?>