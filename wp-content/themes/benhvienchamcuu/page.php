<?php get_header(); ?>
<?php
	use NF\View\Facades\View;
	echo View::render('partials.page');
?>
<?php get_footer(); ?>