<?php

$app = require_once __DIR__ . '/bootstrap/app.php';

//include widget
include_once get_template_directory(). '/app/Widgets/header-text.php';
include_once get_template_directory(). '/app/Widgets/header-contact.php';
include_once get_template_directory(). '/app/Widgets/footer-info.php';
include_once get_template_directory(). '/app/Widgets/contact-online.php';
include_once get_template_directory(). '/app/Widgets/time-work.php';
include_once get_template_directory(). '/app/Widgets/service-doctor-home.php';
include_once get_template_directory(). '/app/Widgets/show-post-category-ask.php';
include_once get_template_directory(). '/app/Widgets/show-post-category-guide.php';
include_once get_template_directory(). '/app/Widgets/show-post-category-introduce.php';
include_once get_template_directory(). '/app/Widgets/news-event.php';

include_once get_template_directory(). '/app/Widgets/doctor-home.php';
include_once get_template_directory(). '/app/Widgets/news-home.php';
include_once get_template_directory(). '/app/Widgets/partner-home.php';
include_once get_template_directory(). '/app/Widgets/Wp-Statistics.php';

//lay img
function tiep_get_thumbnail_url( $size = 'full' ) {
    global $post;
    if (has_post_thumbnail( $post->ID ) ) {
        $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), $size );
        return $image[0];
    }

    // use first attached image
    $images = get_children( 'post_type=attachment&post_mime_type=image&post_parent=' . $post->ID );
    if (!empty($images)) {
        $image = reset($images);
        $image_data = wp_get_attachment_image_src( $image->ID, $size );
        return $image_data[0];
    }

    // use no preview fallback
    if ( file_exists( get_template_directory().'/dist/images/no-image.jpg' ) )
        return get_template_directory_uri().'/dist/images/no-image.jpg';
    else
        return get_template_directory_uri().'/dist/images/no-image.jpg';
}

//lấy title các trang
if (!function_exists('title')) {
    function title()
    {
        if (is_home() || is_front_page()) {
            return get_bloginfo('description');
        }
        if (is_archive()) {
            return get_the_archive_title();
        }
        if (is_search()) {
            return sprintf(__('Search Results for %s', 'sage'), get_search_query());
        }
        if (is_404()) {
            return __('Not Found', 'sage');
        }
        return get_the_title();
    }
}


//custom logo
class setting {
    function __construct(){
        add_theme_support( 'custom-logo' );
    }
}
new setting();


//customizer
function customizer_a( $wp_customize ) {
    // Tạo section
    $wp_customize->add_section (
        'section_a',
        array(
            'title' => 'Khu vực customizer',
            'description' => 'Các tùy chọn cho bệnh viện châm cứu',
            'priority' => 1
        )
    );
    /* Default option */
    $wp_customize->add_setting (
        'text_option',     //id gọi ra
        array(
            'default' => 'Text copyright footer'
        )
    );
    $wp_customize->add_control (
        'control_option1',
        array(
            'type' => 'text',
            'label' => 'Text copyright footer',
            'section' => 'section_a',
            'settings' => 'text_option'
        )
    );
    /* Image Upload */
    $wp_customize->add_setting( 'img-upload' );
    $wp_customize->add_control(
        new WP_Customize_Image_Control(
            $wp_customize,
            'img-upload',
            array(
                'label' => 'Ảnh logo footer',
                'section' => 'section_a',
                'settings' => 'img-upload'
            )
        )
    );
    /* Color Picker */
    $wp_customize->add_setting(
        'color_option',
        array(
            'default' => '#02923d',
        )
    );
    $wp_customize->add_control(
        new WP_Customize_Color_Control(
            $wp_customize,
            'color_option',
            array(
                'label' => 'Màu nền',
                'section' => 'section_a',
                'settings' => 'color_option',
            )
        )
    );
}
add_action( 'customize_register', 'customizer_a' );

//Bộ lọc search
if( !is_admin() ) {
    function filter_search($query) {
        if ($query->is_search) {
            $query->set('post_type', array('chuyen-khoa', 'hop-tac', 'post'));
        };
        return $query;
    };
    add_filter('pre_get_posts', 'filter_search');
}

//cat chuoi
function cut_string($str,$len,$more){
    if ($str=="" || $str==NULL) return $str;
    if (is_array($str)) return $str;
        $str = trim(strip_tags($str));
    if (strlen($str) <= $len) return $str;
        $str = substr($str,0,$len);
    if ($str != "") {
        if (!substr_count($str," ")) {
          if ($more) $str .= " ...";
          return $str;
        }
        while(strlen($str) && ($str[strlen($str)-1] != " ")) {
            $str = substr($str,0,-1);
        }
        $str = substr($str,0,-1);
        if ($more) $str .= " ...";
    }
    return $str;
}


//query sp cua category
function tiep_custom_posttype_query($posttype, $taxonomy, $termId, $numPost){
	$qr =  new WP_Query( array(
			            'post_type' => $posttype,
			            'tax_query' => array(
			                                array(
			                                        'taxonomy' => $taxonomy,
			                                        'field' => 'id',
			                                        'terms' => $termId,
			                                        'operator'=> 'IN'
			                                 )),
			            'showposts'=>$numPost,
			            'order' => 'DESC',
			            'orderby' => 'date'
			     ) );
	return $qr;
}
//phan trang
function tiep_custom_posttype_query_paged($posttype, $taxonomy, $termId, $paged, $size){
	$qr =  new WP_Query( array(
			            'post_type' => $posttype,
			            'tax_query' => array(
			                                array(
			                                        'taxonomy' => $taxonomy,
			                                        'field' => 'id',
			                                        'terms' => $termId,
			                                        'operator'=> 'IN'
			                                 )),
						'posts_per_page' => $size,
						'paged' => $paged,
						'order' => 'DESC',
						'orderby' => 'date'
			     ) );
	return $qr;
}



